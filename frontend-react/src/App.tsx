import { styled } from '@mui/material'
import { useEffect, useState } from 'react'
import './App.css'
import logo from './logo.svg'


const StyledResponse = styled('span')({
  fontWeight: 800
})


const App = () => {

  const [responseFromNode, setResponseFromNode] = useState<{ description: string } | null>(null)

  useEffect(() => {
    fetch(`./api`)
      .then(result => result.json())
      .then(result => {
        setResponseFromNode(result)
      })
  }, [])
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>Response from backend-nodejs: <StyledResponse>{responseFromNode?.description}</StyledResponse></p>
        <a className="App-link" href="./settings">Open Settings</a>
      </header>
    </div>
  )
}

export default App
