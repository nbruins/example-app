# E2e: Getting started

To run the e2e tests local, the packages need to be installed.
Follow thes steps to start testing!

> Make sure nodejs, npm and yarn are installed

Install the packages:

``` yarn
cd e2etests
yarn install --frozen-lockfile
```

## Start test from shell

``` npx
npx playwright test
```

## Start test from vs-code

Make sure the playwright extention is installed. it is added as a recomanded extention in this project

Open the test explorer in vs-code and start a test by clicking on the play button

When no tests are shown somtimes vs-code need to be restarted. Make sure nodejs is available.
To check if it is available run `node --version` in a vs-code buildin terminal.

## Update playwright

Playwright is actively maintained. So new versions are released often. If you want to update the whole project:

``` yarn
yarn upgrade playwright
```

This updates both locally your project as well as the yarn.lock.

## Installing yarn

Installation depends on wheter you want to install it on linux/ubuntu or on windows. Follow for example the instructions on:

<https://www.hostinger.com/tutorials/how-to-install-yarn>
