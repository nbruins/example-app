# Example app

The purpose of this project is to give an example how
to setup CI/CD in Gitlab and automatically deploy to
development and production

## Main directory structure

```bash
.
├── backend-nodejs
│   ├── ...
├── e2etests
│   └── ...
├── frontend-react
│   └── ...
├── helm
│   └── ...
├── utils
│   ├── nginx
│   └── semantic_release
├── .gitlab-ci.yml
├── docker-compose.yml
└── README.md
```

This project consists of a frontend app and a backend api.
They are found in the `backend-nodejs` and `frontend-react` directory.
The apps are both very simple. The frontend is basically one react page
that makes a call to the api and displays the result of the call.
The api is very basic and it returns always the same json

The `helm` directory contains the Helm Charts that are used to deploy
the apps to XCP.

There is als an example of an e2e test in the `e2etest` directory.
The e2e tests are run after every deployment to development

In the `utils` directory there are some helper scripts.

In the root of this project there is the .gitlab-ci.yml with the
Gitlab pipeline definition.
The docker-compose.yml is added to be able to build and run the app
on a local machine with docker.

***

## Getting started

This application has a docker configuration.

Before the first time docker is started a certificate must be
generated. Run the following script in a bash shell and make sure
mkcert is installed.

```bash
cd utils/nginx/certs/ && \
./create-certs.sh
```

When the certificates are generated, start the app
by using the following command:

```bash
docker compose up -d --build
```

### local urls

* frontend: <https://xxllnc.localtest.me>
* api: <https://xxllnc.localtest.me/api>

## More information

* [backend-nodejs](backend-nodejs/README.md)
* [frontend-react](frontend-react/README.md)
* [Helm](helm/README.md)
* [E2e tests](e2etests/README.md)
