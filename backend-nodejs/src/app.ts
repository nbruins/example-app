import express, { Express, Request, Response } from 'express'
import promBundle from 'express-prom-bundle'

export const app: Express = express()

const metricsMiddleware = promBundle({ includeMethod: true })

app.use(metricsMiddleware)

app.get('/api', (_req: Request, res: Response) => {
    res.json({ "description": "This is the response from the api" })
})

// // Handle 404 - Keep this as a last route
app.use((_req, res) => res.status(418).json({
    status: 'error',
    message: 'Er heeft zich een onbekende fout voorgedaan.',
}))
