{{/*
  Function to create a default fully qualified app name.
  We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
  If release name contains chart name it will be used as a full name.
  give $ as the first parameter and name as the second
  Example: {{ include "fullname" (list $ .Values.apps.rootOverviewPage.name) }}
 */}}
{{- define "fullname" -}}
{{- $ := index . 0 }}
{{- $name := index . 1 }}
{{- printf "%s-%s" $.Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end -}}
  
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "selector.labels" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
app.kubernetes.io/name: {{ $name | quote }}
app.kubernetes.io/instance: {{ $.Release.Name | quote}}
{{- end }}

{{/*
Common labels
*/}}
{{- define "common.labels" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
helm.sh/chart: {{ include "chart" $ }}
{{ include "selector.labels" (list $ $name) }}
{{- if $.Chart.AppVersion }}
app.kubernetes.io/version: {{ $.Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ $.Release.Service }}
{{- end }}


{{/*
Create the name of the service account to use
*/}}
{{- define "serviceAccountName" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
{{- if $.Values.global.serviceAccount.create }}
{{- default ( include "fullname" (list $ $name)) $.Values.global.serviceAccount.name }}
{{- else }}
{{- default "default" $.Values.global.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "default_pod_security_context" -}}
runAsNonRoot: true
runAsUser: 1000
{{- end }}

{{- define "liveness.and.readiness.probe" -}}
{{- if .Values.global.livenessProbe.enabled }}
livenessProbe:
  initialDelaySeconds: {{ .Values.global.livenessProbe.initialDelaySeconds }}
  timeoutSeconds: {{ .Values.global.livenessProbe.timeoutSeconds }}
  periodSeconds: {{ .Values.global.livenessProbe.periodSeconds }}
  failureThreshold: {{ .Values.global.livenessProbe.failureThreshold }}
  tcpSocket:
    port: http
{{- end }}
{{- if .Values.global.readinessProbe.enabled }}
readinessProbe:
  initialDelaySeconds: {{ .Values.global.readinessProbe.initialDelaySeconds }}
  timeoutSeconds: {{ .Values.global.readinessProbe.timeoutSeconds }}
  periodSeconds: {{ .Values.global.readinessProbe.periodSeconds }}
  tcpSocket:
    port: http
{{- end }}
{{- end }}