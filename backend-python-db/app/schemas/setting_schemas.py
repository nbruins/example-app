from __future__ import annotations

import uuid as py_uuid
from app.schemas.to_optional import to_optional
from datetime import datetime
from pydantic import BaseModel, Field
from typing import Optional


# Setting
class SettingBase(BaseModel):
    name: str = Field(..., title="Name of the setting")
    value: str = Field(..., title="Value of the setting")
    description: Optional[str] = Field(..., title="Description of the setting")
    regex: Optional[str] = Field(..., title="Regex of the setting")

    class Config:
        allow_population_by_field_name = True


class Setting(SettingBase):
    id: str = Field(..., title="The unique id of the setting")
    uuid: py_uuid.UUID = Field(..., title="The unique uuid of the setting")
    last_modified: Optional[datetime] = Field(
        None, title="Date of last modification", alias="lastModified"
    )

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        extra = "allow"


class SettingCreate(SettingBase):
    pass


class SettingUpdate(to_optional(SettingCreate)):  # type: ignore
    pass


SettingUpdate.update_forward_refs()
