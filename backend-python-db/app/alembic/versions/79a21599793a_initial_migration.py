"""initial migration

Revision ID: 79a21599793a
Revises:
Create Date: 2023-04-24 10:46:13.462976+02:00

"""
import sqlalchemy as sa
from alembic import op
from alembic_utils.pg_function import PGFunction
from sqlalchemy.dialects import postgresql
from sqlalchemy.schema import CreateSequence, Sequence

# from alembic_utils.pg_extension import PGExtension

# revision identifiers, used by Alembic.
revision = "79a21599793a"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # The next line is mannually added to make the sequence for the id
    op.execute(CreateSequence(Sequence("setting_id_seq")))

    # ### commands auto generated by Alembic - please adjust! ###
    public_setting_generate_id = PGFunction(
        schema="public",
        signature="setting_generate_id()",
        definition="RETURNS varchar\n        LANGUAGE plpgsql\n        AS $function$\n            DECLARE\n            v_result      varchar := 'SET-';\n            value_numeric numeric;\n            begin\n                SELECT nextval('\"setting_id_seq\"') INTO value_numeric;\n                IF value_numeric > 9999 THEN\n                    v_result := v_result || value_numeric;\n                ELSE\n                    v_result := v_result || to_char(value_numeric, 'FM0000');\n                END IF;\n                return v_result;\n            END;\n        $function$",  # noqa: E501
    )
    op.create_entity(public_setting_generate_id)

    op.create_table(
        "setting",
        sa.Column("sid", sa.Integer(), nullable=False),
        sa.Column(
            "id", sa.String(), server_default=sa.text("setting_generate_id()"), nullable=False
        ),
        sa.Column(
            "uuid",
            postgresql.UUID(as_uuid=True),
            server_default=sa.text("gen_random_uuid()"),
            nullable=False,
        ),
        sa.Column("name", sa.String(), nullable=True),
        sa.Column("value", sa.String(), nullable=True),
        sa.Column("description", sa.String(), nullable=True),
        sa.Column("regex", sa.String(), nullable=True),
        sa.Column("last_modified", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("sid"),
        sa.UniqueConstraint("uuid"),
    )
    op.create_index(op.f("ix_setting_id"), "setting", ["id"], unique=True)
    op.create_index(op.f("ix_setting_name"), "setting", ["name"], unique=False)
    op.create_index(op.f("ix_setting_sid"), "setting", ["sid"], unique=False)

    # public_uuid_ossp = PGExtension(schema="public", signature="uuid-ossp")
    # op.drop_entity(public_uuid_ossp)

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    # public_uuid_ossp = PGExtension(schema="public", signature="uuid-ossp")
    # op.create_entity(public_uuid_ossp)

    public_setting_generate_id = PGFunction(
        schema="public",
        signature="setting_generate_id()",
        definition="RETURNS varchar\n        LANGUAGE plpgsql\n        AS $function$\n            DECLARE\n            v_result      varchar := 'SET-';\n            value_numeric numeric;\n            begin\n                SELECT nextval('\"setting_id_seq\"') INTO value_numeric;\n                IF value_numeric > 9999 THEN\n                    v_result := v_result || value_numeric;\n                ELSE\n                    v_result := v_result || to_char(value_numeric, 'FM0000');\n                END IF;\n                return v_result;\n            END;\n        $function$",  # noqa: E501
    )
    op.drop_entity(public_setting_generate_id)

    op.drop_index(op.f("ix_setting_sid"), table_name="setting")
    op.drop_index(op.f("ix_setting_name"), table_name="setting")
    op.drop_index(op.f("ix_setting_id"), table_name="setting")
    op.drop_table("setting")
    # ### end Alembic commands ###
