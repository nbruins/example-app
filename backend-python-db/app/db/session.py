# backend-python-db/app/db/session.py
import boto3
from app.core import config
from sqlalchemy import URL, create_engine, event
from sqlalchemy.orm import DeclarativeBase, sessionmaker


class Base(DeclarativeBase):
    pass


client = boto3.client("rds", region_name=config.AWS_REGION_NAME)


def get_token(host: str = config.DBHOSTNAME):
    if config.DBPASSWORD:
        return config.DBPASSWORD

    return client.generate_db_auth_token(
        DBHostname=host,
        Port=config.DBPORT,
        DBUsername=config.DBUSER,
        Region=config.AWS_REGION_NAME,
    )


def get_url(host: str = config.DBHOSTNAME, add_password: bool = True):
    return URL.create(
        "postgresql",
        username=config.DBUSER,
        password=get_token() if add_password else None,
        host=host,
        database=config.DBNAME,
        port=config.DBPORT,
    )


def _create_engine(url: URL):
    return create_engine(
        url=url,
        future=True,
        pool_recycle=300,
        echo=config.SQLALCHEMY_ECHO,
        pool_size=config.SQLALCHEMY_POOL_SIZE,
        max_overflow=config.SQLALCHEMY_POOL_MAX_OVERFLOW,
    )


engine = _create_engine(url=get_url(host=config.DBHOSTNAME, add_password=False))
write_session = sessionmaker(bind=engine, future=True, autocommit=False, autoflush=False)

engine_ro = _create_engine(url=get_url(host=config.DBROHOSTNAME, add_password=False))
readonly_session = sessionmaker(bind=engine_ro, future=True, autocommit=False, autoflush=False)


def get_read_db():
    db = readonly_session()
    try:
        yield db
    finally:
        db.close()


def get_write_db():
    db = write_session()
    try:
        yield db
    finally:
        db.close()


@event.listens_for(engine, "do_connect")
def provide_token(dialect, conn_rec, cargs, cparams):
    cparams["password"] = get_token()


@event.listens_for(engine_ro, "do_connect")
def provide_ro_token(dialect, conn_rec, cargs, cparams):
    cparams["password"] = get_token(host=config.DBROHOSTNAME)
