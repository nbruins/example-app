import os
import sys
from dotenv import load_dotenv
from typing import Final, Union

if "pytest" in sys.modules:
    load_dotenv(".env.test")


def to_int(value: Union[str, None], default_value: int) -> int:
    try:
        return int(value) if value else default_value
    except ValueError:
        return default_value


# The title of the API
PROJECT_NAME = "backend-python-db"
BASE_PATH: Final[str] = "/python-api"
BASE_PATH_V1: Final[str] = f"{BASE_PATH}/v1"

#############################################
# SQLAlchemy settings
#############################################
# If set to True, all database queries will be logged as they are executed.
SQLALCHEMY_ECHO: Final[bool] = os.environ.get("DATABASE_ECHO", "False").upper() == "TRUE"
SQLALCHEMY_POOL_SIZE: Final[int] = to_int(os.environ.get("SQLALCHEMY_POOL_SIZE", None), 5)
SQLALCHEMY_POOL_MAX_OVERFLOW: Final[int] = to_int(
    os.environ.get("SQLALCHEMY_POOL_MAX_OVERFLOW", None), 10
)

#############################################
# DB settings
#############################################
DBHOSTNAME: Final[str] = os.environ["DBHOSTNAME"]
DBROHOSTNAME: Final[str] = os.environ["DBROHOSTNAME"]
DBPORT: Final[int] = to_int(os.environ["DBPORT"], 5432)
DBUSER: Final[str] = os.environ["DBUSER"]
DBNAME: Final[str] = os.environ["DBNAME"]
DBPASSWORD: Final[str | None] = os.environ.get("DBPASSWORD", None)

# Value for max-age in Cache-Control HTTP header for App icon
APP_ICON_CACHE_SECONDS = int(os.environ.get("APP_ICON_CACHE_SECONDS", 3600))

# Future: OIDC parameters should be looked up in some kind of table
# so we can handle customer logins using their own SSO solutions

# Maximum number of results from a search. To retrieve more, use paging.
MAX_PAGE_SIZE = int(os.environ.get("MAX_PAGE_SIZE", "100"))

# Flag to enable or disable audit logging
AUDIT_LOGGING = bool(os.environ.get("AUDIT_LOGGING", True))

# AWS Settings
AWS_REGION_NAME: Final[str] = os.environ.get("AWS_ACCESS_KEY_ID", "eu-west-1")
AWS_ACCESS_KEY_ID: Final[str | None] = os.environ.get("AWS_ACCESS_KEY_ID", None)
AWS_SECRET_ACCESS_KEY: Final[str | None] = os.environ.get("AWS_SECRET_ACCESS_KEY", None)
